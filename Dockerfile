# Dockerfile for anchorectl demonstration

# use alpine:latest for a smaller image, but it often won't have any published CVEs
FROM alpine:latest
LABEL maintainer="pvn@novarese.net"
LABEL name="test_distributed_scan"
LABEL org.opencontainers.image.title="test_distributed_scan"
LABEL org.opencontainers.image.description="Simple image to test anchorectl with Anchore Enterprise."

COPY anchore_hints.json /

RUN set -ex && \
    echo "aws_access_key_id=01234567890123456789" > /aws_access && \
    echo "-----BEGIN OPENSSH PRIVATE KEY-----" > /ssh_key && \
    apk add --no-cache curl jq sudo && \
    curl -sSfL https://raw.githubusercontent.com/anchore/syft/main/install.sh | sh -s -- -b /usr/local/bin  && \
    curl -sSfL https://raw.githubusercontent.com/anchore/grype/main/install.sh | sh -s -- -b /usr/local/bin         

# use date to force a unique build every time
RUN date > /image_build_timestamp
ENTRYPOINT /bin/false


